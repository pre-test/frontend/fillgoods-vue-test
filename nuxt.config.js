
module.exports = {
  mode: 'universal',
  srcDir: 'src/',
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'en'
    },
    titleTemplate: process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
 ** Router
 */
  router: {
    middleware: 'redirect',
    extendRoutes(routes, resolve) {
      // Automatically map all route params to component props:
      for (const route of routes) {
        route.props = /:/.test(route.path)
      }
    }
  },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/firebase.plugin.js',
    // { src: '~/plugins/firebase.plugin.js', ssr: false },
    { src: '~/plugins/checkAuth.js', ssr: false }
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: '/api',
    credentials: true,
    proxy: false,
    debug: process.env.NODE_ENV !== 'production',
    requestInterceptor: (config, { store }) => {
      config.headers.common['Authorization'] = ''
      config.headers.common['Content-Type'] = 'application/json'
      return config
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  serverMiddleware: [
    // API middleware
    '~/services/api'
  ]
}
