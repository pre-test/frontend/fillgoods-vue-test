import Vuex from 'vuex'
import authModule from './modules/authStorage'

export default () => new Vuex.Store({
  stateFactory: true,
  namespaced: true,
  modules: {
    authStorage: authModule
  }
})
