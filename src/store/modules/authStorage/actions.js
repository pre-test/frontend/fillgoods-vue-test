export default {
  setSignIn({ commit }, profile) {
    commit('addProfile', profile)
  },
  setSignOut({ commit }) {
    commit('clearProfile')
  }
}
