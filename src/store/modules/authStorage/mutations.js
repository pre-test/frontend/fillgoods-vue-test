export default {
  addProfile(state, profile) {
    state.profile = profile
  },
  clearProfile(state) {
    state.profile = {}
  }
}
