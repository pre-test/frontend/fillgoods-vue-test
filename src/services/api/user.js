const express = require('express')
require('../../plugins/firebase.plugin')
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
const auth = firebase.auth()
const db = firebase.firestore()
// const Http = require('../../http')

const router = express()
// get by user
router.get('/user/profile', (req, res) => {
  const uid = req.get('Authorization')
  db.collection('users').doc(uid).get()
    .then((response) => {
      res.send({ status: 200, data: response.data()})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
// patch profile
router.patch('/user/update-profile', (req, res) => {
  const uid = req.get('Authorization')
  db.collection('users').doc(uid).update({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    age: req.body.age,
    tel: req.body.tel,
    address: req.body.address
  })
    .then((response) => {
      res.send({ status: 200, data: { massage: "แก้ไขข้อมูลสำเร็จ"}})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
// create user
router.post('/user/create-user', (req, res) => {
  auth.createUserWithEmailAndPassword(req.body.email, req.body.password)
    .then((response) => {
      db.collection('users').doc(response.user.uid).set({
        role: 'user',
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        age: req.body.age,
        tel: req.body.tel,
        address: req.body.address
      })
      res.send({ status: 200, data: { massage: "ลงทะเบียนสำเร็จ"}})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
// get all user
router.get('/user/all-user', (req, res) => {
  db.collection('users').where("role", '==', 'user').get()
    .then((response) => {
      // console.log(response.docs)
      let profiles = []
      response.docs.map((doc) => {
        const id = { id: doc.id }
        profiles.push({ ...id, ...doc.data()})
      })
      res.send({ status: 200, data: profiles})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
// patch user profile
router.patch('/user/update-user-profile', (req, res) => {
  const uid = req.body.id
  db.collection('users').doc(uid).update({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    age: req.body.age,
    tel: req.body.tel,
    address: req.body.address
  })
    .then((response) => {
      res.send({ status: 200, data: { massage: "แก้ไขข้อมูลสำเร็จ"}})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
module.exports = router
