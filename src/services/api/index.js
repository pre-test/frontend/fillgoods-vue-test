// Require API routes
const { Router } = require('express')
const bodyParser = require('body-parser')
const auth = require('./auth')
const user = require('./user')

const router = Router()

router.use(bodyParser.json({ limit: '50mb' })) // for parsing application/json
router.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 })) // for parsing application/x-www-form-urlencoded


// Add USERS Routes
router.use(auth)
router.use(user)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: router
}
