const express = require('express')
require('../../plugins/firebase.plugin')
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
const auth = firebase.auth()
// const db = firebase.firestore()
// const Http = require('../../http')

const router = express()

router.post('/login', (req, res) => {
  auth.signInWithEmailAndPassword(req.body.email, req.body.password)
    .then((data) => {
      res.send({ status: 200, data: data })
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})

router.get('/logout', (req, res) => {
  auth.signOut()
    .then((data) => {
      let dataObj = {
        is_login: false,
        message: "ออกจากระบบเรียบร้อยแล้ว"
      }
      res.send({ status: 200, data: dataObj})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
module.exports = router
