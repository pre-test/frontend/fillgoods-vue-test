import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/performance'

// export default (ctx, inject) => {
//   if (!firebase.apps.length) {
//     var firebaseConfig = {
//       apiKey: "AIzaSyAgLEbwEcRqcQcwbxon1RLJQmzzuiBzz9A",
//       authDomain: "test-vue-nuxt-auth.firebaseapp.com",
//       databaseURL: "https://test-vue-nuxt-auth.firebaseio.com",
//       projectId: "test-vue-nuxt-auth",
//       storageBucket: "test-vue-nuxt-auth.appspot.com",
//       messagingSenderId: "954242224231",
//       appId: "1:954242224231:web:9d2fd185874f7a76dc84c2",
//       measurementId: "G-XHFGGYE8E3"
//     };
//     firebase.initializeApp(firebaseConfig);
//   }
//
//   const perf = firebase.performance()
//   inject('FirebasePerf', perf)
// }

  const firebaseConfig = {
    apiKey: "AIzaSyAgLEbwEcRqcQcwbxon1RLJQmzzuiBzz9A",
    authDomain: "test-vue-nuxt-auth.firebaseapp.com",
    databaseURL: "https://test-vue-nuxt-auth.firebaseio.com",
    projectId: "test-vue-nuxt-auth",
    storageBucket: "test-vue-nuxt-auth.appspot.com",
    messagingSenderId: "954242224231",
    appId: "1:954242224231:web:9d2fd185874f7a76dc84c2",
    measurementId: "G-XHFGGYE8E3"
  }

  let app = null
  if (!firebase.apps.length) {
    app = firebase.initializeApp(firebaseConfig);
  }

  export default firebase
