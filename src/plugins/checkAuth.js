import firebase from "firebase";
import 'firebase/auth';
import 'firebase/firestore';

export default ({ $axios, store }) => {
  const auth = firebase.auth()
  return new Promise((resolve, reject) => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        // Sign in
        let profile = store.getters['auth/getProfile']
        if (Object.keys(profile).length === 0) {
          let newProfile = {
            email: user.email,
            uid: user.uid,
            emailVerified: user.emailVerified
          }
          $axios.defaults.headers = {
            'Content-Type': 'application/json',
            'Authorization': user.uid
          }
          return resolve(store.dispatch('setSignIn', newProfile))
        }
      } else  {
        // Sign out
        return resolve(store.dispatch('setSignOut'))
      }
    })
  })
}
